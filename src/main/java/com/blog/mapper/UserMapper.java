package com.blog.mapper;

import com.blog.pojo.User;
import tk.mybatis.mapper.common.Mapper;

public interface UserMapper extends Mapper<User> {

    User selectByUsername(String username);
}