package com.blog.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Bean
    public PasswordEncoder getPasswordEncoder(){
        return new BCryptPasswordEncoder();
    }
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //关闭csrf 可以理解成防火墙
        http.csrf().disable();

        http.authorizeRequests()
                .antMatchers("/userLogin").permitAll()
                .antMatchers("/toSuccess").permitAll()
                .antMatchers("/toError").permitAll()
                //放行静态资源
                .antMatchers("/*.html").permitAll()
                .anyRequest().authenticated();  //其他请求都需要认证

        http.formLogin()    //开启表单验证
                .loginProcessingUrl("/login").permitAll()
                .loginPage("/userLogin")
                .successForwardUrl("/toSuccess")
                .failureForwardUrl("/toError");

        /*//记住我
        http.rememberMe()
                .tokenValiditySeconds(60*60*24)   //设置过期时间  一天
                .userDetailsService(userDetailService)      //自定义登录逻辑
                .tokenRepository(tokenRepository);*/

        //注销
        http.logout()
                //.addLogoutHandler()     //自定义注销处理器
                .logoutUrl("/logout")   //填写该地址后springsecurity会自动帮你处理注销
                .logoutSuccessUrl("/userLogin");
    }
}