package com.blog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import tk.mybatis.spring.annotation.MapperScan;

@SpringBootApplication
@MapperScan("com.blog.mapper")
public class WlBlogApplication {

    public static void main(String[] args) {
        SpringApplication.run(WlBlogApplication.class, args);
    }

}
