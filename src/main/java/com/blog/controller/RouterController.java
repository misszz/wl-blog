package com.blog.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class RouterController {

    @RequestMapping("index")
    public String index(){
        return "index";
    }

    @RequestMapping("/toSuccess")
    public String toSuccess(){
        return "index";
    }
    @RequestMapping("/toError")
    public String toError(){
        return "error";
    }

    @RequestMapping("/test")
    public String test(){
        return "test";
    }

}
