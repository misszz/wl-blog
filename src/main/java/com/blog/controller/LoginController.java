package com.blog.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author wangzhongxu
 * @version 1.0
 * *@description
 * @since 2020/8/18 21:45
 **/
@Controller
public class LoginController {
    @RequestMapping("userLogin")
    public String login(){
        return "login";
    }
}
