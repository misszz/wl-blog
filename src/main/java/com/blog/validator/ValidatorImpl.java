package com.blog.validator;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Set;

/**
 * @author wangzhongxu
 * @version 1.0
 * *@description
 * @since 2020/5/7 10:04
 **/
@Component
public class ValidatorImpl implements InitializingBean {
    private Validator validator;

    //实现校验方法并返回校验结果
    public ValidationResult validator(Object bean) {
       ValidationResult result = new ValidationResult();
        Set<ConstraintViolation<Object>> constraintViolations = validator.validate(bean);
        if (constraintViolations.size() > 0) {
            //有错误
            result.setHasErrors(true);
            constraintViolations.forEach(constraintViolation ->{
                String errMsg = constraintViolation.getMessage();
                String propertityName = constraintViolation.getPropertyPath().toString();
                result.getErrMsgMap().put(propertityName,errMsg);
            });
        }
        return result;
    }

    //spring初始化完成后会回调该方法
    @Override
    public void afterPropertiesSet() throws Exception {
        //将hibernate vavlidator通过工厂的初始化方式使其实例化
        this.validator = Validation.buildDefaultValidatorFactory().getValidator();

    }
}
