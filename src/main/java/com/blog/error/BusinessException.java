package com.blog.error;

/**
 * @author wangzhongxu
 * @version 1.0
 * *@description 使用了包装器的思想
 * @since 2020/5/2 16:25
 **/

public class BusinessException extends Exception implements CommonError {
    private CommonError commonError;
    //直接接受EnumBusinessError的传参，用于构造业务异常
    public BusinessException(CommonError commonError) {
        super();
        this.commonError = commonError;
    }
    //接受自定义errMsg的方法构造业务异常
    public BusinessException(CommonError commonError,String errMsg){
        super();
        this.commonError = commonError;
        this.commonError.setErrMsg(errMsg);
    }
    @Override
    public int getErrCode() {
        return this.commonError.getErrCode();
    }

    @Override
    public String getErrMsg() {
        return this.commonError.getErrMsg();
    }

    @Override
    public CommonError setErrMsg(String errMsg) {
        this.commonError.setErrMsg(errMsg);
        return this;
    }

    @Override
    public String toString() {
        return "BusinessException{" +
                "commonError=" + commonError +
                '}';
    }
}
