package com.blog.error;

/**
 * @author wangzhongxu
 * @version 1.0
 * *@description
 * @since 2020/5/2 16:01
 **/

public interface CommonError {
    public int getErrCode();
    public String getErrMsg();
    public CommonError setErrMsg(String errMsg);
}
