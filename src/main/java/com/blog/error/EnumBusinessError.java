package com.blog.error;

/**
 * @author wangzhongxu
 * @version 1.0
 * *@description
 * @since 2020/5/2 16:07
 **/

    public enum  EnumBusinessError implements CommonError {
    //通用错误类型00001
    PARAMETER_VALIDATION_ERROR(10001,"参数不合法"),
    UNKNOWN_ERROR(10002,"未知错误"),
    PARAMETER_NOT_NULL(10003,"参数不能为空"),
    //以2开头为用户信息相关错误定义
    USER_NOT_EXIST(20001,"用户不存在"),
    TELPHONE_NOT_EXIST(20002,"手机号码或密码不正确"),
    USER_NOT_LOGIN(2003,"用户未登录"),
    //以3开头为商品信息相关错误定义
    PRODUCT_NOT_EXIST(30001,"商品不存在"),
    //以4开头为库存信息相关错误定义
    STOCK_NOT_ENOUGH(40001,"库存量不足"),
    ;

    EnumBusinessError(int errCode, String errMsg) {
        this.errCode = errCode;
        this.errMsg = errMsg;
    }

    private int errCode;
    private String errMsg;
    @Override
    public int getErrCode() {
        return this.errCode;
    }

    @Override
    public String getErrMsg() {
        return this.errMsg;
    }

    @Override
    public CommonError setErrMsg(String errMsg) {
        this.errMsg = errMsg;
        return this;
    }

    @Override
    public String toString() {
        return "EnumBusinessError{" +
                "errCode=" + errCode +
                ", errMsg='" + errMsg + '\'' +
                '}';
    }

    public static void main(String[] args) {
        try {
            throw new BusinessException(EnumBusinessError.UNKNOWN_ERROR);
        } catch (BusinessException e) {
            e.printStackTrace();
        }
    }
}
