package com.blog.service.impl;

import com.blog.mapper.UserMapper;
import com.blog.pojo.User;
import com.blog.service.UserService;
import com.blog.validator.ValidationResult;
import com.blog.validator.ValidatorImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author wangzhongxu
 * @version 1.0
 * *@description
 * @since 2020/8/18 22:36
 **/
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private ValidatorImpl validator;
    @Override
    public void register(User user) {
        ValidationResult result = this.validator.validator(user);

    }
}
