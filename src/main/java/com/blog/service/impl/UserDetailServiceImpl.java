package com.blog.service.impl;

import com.blog.mapper.UserMapper;
import com.blog.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * 用户点击登录按钮时会调用里面的方法
 */
@Service
public class UserDetailServiceImpl implements UserDetailsService {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        //1.判断用户是否存在
        User user = userMapper.selectByUsername(username);
        if (user == null){
            throw new UsernameNotFoundException("用户不存在");
        }
        //2.验证密码是否正确
        String encode = passwordEncoder.encode("123");
        System.out.println("encode = " + encode);
        //3.获取用户权限
        return new org.springframework.security.core.userdetails.User(user.getAccount(),user.getPassword(), AuthorityUtils.commaSeparatedStringToAuthorityList("abc"));
    }
}
